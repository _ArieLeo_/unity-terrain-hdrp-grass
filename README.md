# Unity Terrain HDRP Grass

A demonstration of using GPU instancing for HDRP grass.
You can use the C# scripts in HDRPTerrainGrass/Assets/Scripts and models/textures in HDRPTerrainGrass/Assets/Grass and Blender models folders under the MIT License.

Let me know if you do! (https://fd-research.com/contact/) 

Here is a screenshot of the sample scene, with 45FPS on my current system:

* GPU: NVIDIA GTX 1070 (crappy, old)
* CPU: AMD Ryzen 7 3700X 8-Core (new)
* Unity Version: 2020.3.14f1.713 Personal (this is the LTS 2020.3 version); HDRP lighting.

<img src="wheatbackground.png" alt="wheatbackground" style="zoom:50%;" />

The terrain system will complain that "the current render pipeline does not support Detail shaders", but if you press "Play", the instancing will work anyway, with the standard HDRP/Lit shader.

![terraindetails](terraindetails.png)

Rudimentary wind is also implemented, you have to use a Wind zone and link it to the TerrainDetailInstancing script added to the terrain object:

<img src="terraindetailsinstancing.png" alt="terraindetailsinstancing" style="zoom:100%;" />

These are the public members of the TerrainDetailInstancing.cs script:

- MyInstancer: link to a prefab with the Instancing.cs script on it (see SampleScene)
- Detail Density: [0,1] float that controls the detail density on the terrain. This is a copy of the terrain detail density, necessary because the terrain detail density float is set to zero to remove the Unity details on the terrain (they do not use the correct shader).
- Update Interval: [0,1] float that controls how often the Instancing object positions are updated (per second). Lowering this value makes the camera position updates look smoother, but reduces framerate.
- Fade Details At Distance: [true/false] bool that enables distance based fading of objects.
- Fade Range: float (meters), how far from the terrain Detail Distance (in the terrain object) the fading effect is visible, if Fade Details At Distance is true.
- Wind Zone: if None, the wind effect will not be used. If set to a Unity Wind Zone object, this controls the wind effect (implemented in Scripts/Instancing.cs).
