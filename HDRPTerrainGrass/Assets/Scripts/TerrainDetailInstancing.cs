using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Terrain))]
// [ExecuteAlways]
public class TerrainDetailInstancing : MonoBehaviour
{
    public Instancing myInstancer;
    [Range(0, 1f)]
    public float DetailDensity = 1f;
    [Range(0, 1f)]
    public float UpdateInterval = 1f;
    public bool FadeDetailsAtDistance = true;
    [Range(0.1f, 100f)]
    public float FadeRange = 4;

    public WindZone WindZone;

    private struct InstanceData
    {
        public List<Vector3> positions;
        public List<Vector3> scales;
        public List<Quaternion> rotations;
        public List<float> windStrengths;
    }

    private Terrain terrain;
    private Instancing[] instancer;
    private Dictionary<int, InstanceData> detailData = new Dictionary<int, InstanceData>();
    private float lastUpdateTime = float.MinValue;

    void Start()
    {
        terrain = GetComponent<Terrain>();

        // this is to remove the detail object meshes that unity draws itself
        terrain.detailObjectDensity = 0;

        var details = terrain.terrainData.detailPrototypes;

        instancer = new Instancing[details.Length];
        for (int i = 0; i < details.Length; i++)
        {
            var mf = details[i].prototype.GetComponent<MeshFilter>();
            var mr = details[i].prototype.GetComponent<MeshRenderer>();
            if (mf == null)
            {
                // try first lod
                if(details[i].prototype.GetComponent<LODGroup>() != null && details[i].prototype.GetComponent<LODGroup>().GetLODs().Length > 0)
                {
                    if (details[i].prototype.GetComponent<LODGroup>().GetLODs()[0].renderers.Length > 0)
                    {
                        mf = details[i].prototype.GetComponent<LODGroup>().GetLODs()[0].renderers[0].GetComponent<MeshFilter>();
                        mr = details[i].prototype.GetComponent<LODGroup>().GetLODs()[0].renderers[0].GetComponent<MeshRenderer>();
                    }
                }
            }

            if (mf != null && mr != null)
            {
                instancer[i] = Instantiate<Instancing>(myInstancer, Vector3.zero, Quaternion.identity, this.transform);
                instancer[i].MeshFilter = mf;
                instancer[i].Material = mr.sharedMaterial;
            }
            else
            {
                Debug.LogError("Detail object #" + i + " is missing MeshRenderer or MeshFilter.");
            }
        }
    }

    public void Update()
    {
        if(Time.time - lastUpdateTime < UpdateInterval)
        {
            return;
        }
        lastUpdateTime = Time.time;

        // reinitialize all instancers
        detailData.Clear();
        for (int iLayer = 0; iLayer < terrain.terrainData.detailPrototypes.Length; iLayer++)
        {
            if (instancer[iLayer] != null)
            {
                instancer[iLayer].WindZone = WindZone;
                var prototype = terrain.terrainData.detailPrototypes[iLayer];
                RecomputeDetailData(iLayer);
                instancer[iLayer].InitData(
                    detailData[iLayer].positions.ToArray(),
                    detailData[iLayer].rotations.ToArray(),
                    detailData[iLayer].scales.ToArray(),
                    detailData[iLayer].windStrengths.ToArray(),
                    prototype.healthyColor);
            }
        }
    }

    /// <summary>
    /// Recompute the InstanceData for every single DetailMesh in the range of the Camera.main,
    /// for the given prototype (i.e. for a single detail mesh type).
    /// The recomputed InstanceData is sent to the corresponding Instancing object.
    /// </summary>
    /// <param name="prototypeIndex">The index of the prototype detail mesh.</param>
    private void RecomputeDetailData(int prototypeIndex)
    {
        // prepare all the necessary information
        var detailPrototype = terrain.terrainData.detailPrototypes[prototypeIndex];
        var detailWidth = terrain.terrainData.detailWidth;
        var detailMap = terrain.terrainData.GetDetailLayer(0, 0, detailWidth, detailWidth, prototypeIndex);
        var noiseSpread = detailPrototype.noiseSpread;
        var cameraPos = Camera.main.transform.position;
        var detailDistance = terrain.detailObjectDistance;

        var localDetailData = new InstanceData()
        {
            positions = new List<Vector3>(),
            scales = new List<Vector3>(),
            rotations = new List<Quaternion>(),
            windStrengths = new List<float>()
        };
        var detailScale = new Vector2(1f / detailWidth * terrain.terrainData.size.x, 1f / detailWidth * terrain.terrainData.size.z);

        var detailWidthDistance = Mathf.Max(Mathf.CeilToInt(detailDistance / detailScale.x),
                                            Mathf.CeilToInt(detailDistance / detailScale.y));
        // We have to flip x and y of the camera pos, since ix and iz iterate over an image.
        var detailCameraPos = new Vector2((cameraPos.z-terrain.transform.position.z) / detailScale.y,
                                          (cameraPos.x-terrain.transform.position.x) / detailScale.x);
        var ixStart = Mathf.Max(0, Mathf.FloorToInt(detailCameraPos.x - detailWidthDistance));
        var izStart = Mathf.Max(0, Mathf.FloorToInt(detailCameraPos.y - detailWidthDistance));
        var ixEnd = Mathf.Min(detailWidth, ixStart + 2*detailWidthDistance);
        var izEnd = Mathf.Min(detailWidth, izStart + 2*detailWidthDistance);
        var detailSpaceMax = Mathf.Max(detailDistance / detailScale.y, detailDistance / detailScale.x);

        // now, process all the detail cells in range of the Camera.main
        for (int ix = ixStart; ix < ixEnd; ix+=1)
        {
            for (int iz = izStart; iz < izEnd; iz+=1)
            {
                // If the current cell is not in the DetailWidth radius, skip it.
                var detailSpaceDistanceX = (ix - detailCameraPos.x) * (ix - detailCameraPos.x);
                var detailSpaceDistanceZ = (iz - detailCameraPos.y) * (iz - detailCameraPos.y);
                var detailSpaceDistance = detailSpaceDistanceX + detailSpaceDistanceZ;
                if (detailSpaceDistance > detailSpaceMax * detailSpaceMax)
                {
                    continue;
                }

                // fix the random seed based on the current cell, so that the detail meshes for single cells are
                // always constructed the same way.
                UnityEngine.Random.InitState(ix * 7919 + iz * 5309 + prototypeIndex * 3359);
                if (detailMap[ix, iz] > 0 && UnityEngine.Random.value < DetailDensity)
                {
                    var pos = new Vector3();
                    pos.x = detailScale.x * iz + (detailScale.x * 0.5f);
                    pos.z = detailScale.y * ix + (detailScale.y * 0.5f);

                    pos.x -= (detailScale.x * (UnityEngine.Random.value -.5f) * noiseSpread);
                    pos.z += (detailScale.y * (UnityEngine.Random.value - .5f) * noiseSpread);

                    pos.y = terrain.SampleHeight(pos);

                    var cameraVector = cameraPos - pos;
                    var distance = cameraVector.magnitude;
                    if (distance < detailDistance && Vector3.Dot(Camera.main.transform.forward, cameraVector) < -.1f)
                    {
                        var scaleHeight = detailPrototype.minHeight + UnityEngine.Random.value * (detailPrototype.maxHeight - detailPrototype.minHeight);
                        var scaleWidth = detailPrototype.minWidth + UnityEngine.Random.value * (detailPrototype.maxWidth - detailPrototype.minWidth);

                        var fadeDistance = Mathf.Clamp01((detailDistance - distance) / FadeRange);
                        var scaleFade = FadeDetailsAtDistance ? Mathf.Lerp(0, 1, fadeDistance) : 1;

                        localDetailData.positions.Add(pos);
                        localDetailData.scales.Add(scaleFade * new Vector3(scaleWidth, scaleHeight, scaleWidth));
                        localDetailData.windStrengths.Add(detailPrototype.dryColor.grayscale);

                        var rotation = Quaternion.Euler(0, UnityEngine.Random.value * 360, 0);
                        localDetailData.rotations.Add(rotation);
                    }
                }
            }
        }

        detailData.Add(prototypeIndex, localDetailData);
    }
}
