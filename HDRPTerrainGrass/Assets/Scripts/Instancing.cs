﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using System;
using System.Linq;

public class Instancing : MonoBehaviour
{
    public ShadowCastingMode castShadows = ShadowCastingMode.On;

    public bool turnOnInstance = true;
    public bool receiveShadows = false;

    public int Count = 2;

    public MeshFilter MeshFilter;
    public Material Material;

    public WindZone WindZone;

    private Mesh mesh;

    private bool isInitialized = false;

    private readonly int BATCH_SIZE = 1023;
    private const float BATCH_SIZE_FLOAT = 1023f;
    private Matrix4x4[] matrices;

    private Vector3[] positions;
    private Quaternion[] rotations;
    private Vector3[] scales;
    private float[] windStrengths;
    private Color color;

    private MaterialPropertyBlock materialProperty;

    void Start()
    {
        mesh = MeshFilter.sharedMesh;
    }

    public void InitData(Vector3[] positions, Quaternion[] rotations, Vector3[] scales, float[] windStrengths, Color color)
    {
        Count = positions.Length;

        materialProperty = new MaterialPropertyBlock();
        materialProperty.SetColor("_BaseColor", color);

        matrices = new Matrix4x4[Count];
        this.positions = positions;
        this.rotations = rotations;
        this.scales = scales;
        this.color = color;
        this.windStrengths = windStrengths;

        isInitialized = true;

        if(WindZone == null)
        {
            InitMatrices(positions, rotations, scales);
        }
    }

    private void InitMatrices(Vector3[] positions, Quaternion[] rotations, Vector3[] scales)
    {
        for (int idx = 0; idx < Count; idx++)
        {
            matrices[idx] = Matrix4x4.identity;
            matrices[idx].SetTRS(positions[idx], rotations[idx], scales[idx]);
        }

        isInitialized = true;
    }

    void Update()
    {
        if (turnOnInstance && this.isInitialized)
        {
            int total = matrices.Length;
            int batches = Mathf.CeilToInt(total / BATCH_SIZE_FLOAT);
            var windRotations = new Quaternion[Count];

            if (WindZone != null)
            {
                UnityEngine.Random.InitState(1);
                for (int idx = 0; idx < positions.Length; idx++)
                {
                    windRotations[idx] = ApplyWind(positions[idx], rotations[idx], windStrengths[idx]);
                }
                InitMatrices(positions, windRotations, scales);
            }

            for (int i = 0; i < batches; i++)
            {
                int batchCount = Mathf.Min(BATCH_SIZE, total - (BATCH_SIZE * i));
                int start = Mathf.Max(0, i * BATCH_SIZE);

                var batchedMatrices = matrices.Skip(start).Take(batchCount).ToList();
                Graphics.DrawMeshInstanced(mesh, 0, Material, batchedMatrices, materialProperty, castShadows);
            }
        }
    }

    private Quaternion ApplyWind(Vector3 position, Quaternion rotation, float strength = 1)
    {
        if (WindZone == null)
            return rotation;

        var windShift = WindZone.windPulseMagnitude * Mathf.PerlinNoise(position.x * WindZone.windTurbulence, position.z * WindZone.windTurbulence);
        var mainWind = Mathf.Sin(Time.time * WindZone.windPulseFrequency + 2 * Mathf.PI * windShift);
        var randomWind = Mathf.Cos(Time.time * (UnityEngine.Random.value - .5f) * 2 * Mathf.PI * WindZone.windTurbulence) * windShift * WindZone.windTurbulence;
        var waveGrassAmount = WindZone.windPulseMagnitude * 10;
        var windRotation = Quaternion.AngleAxis(strength * WindZone.windMain * (mainWind + randomWind) * waveGrassAmount, WindZone.transform.right);
        return windRotation * rotation;
    }
}
